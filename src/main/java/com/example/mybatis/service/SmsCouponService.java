package com.example.mybatis.service;

import com.example.mybatis.domain.SmsCoupon;

/**
 * @author shanks on 2019-11-09
 */
public interface SmsCouponService {
    SmsCoupon querySmsCouponDetail(int id);
}
