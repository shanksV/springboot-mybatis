package com.example.mybatis.service.impl;

import com.example.mybatis.domain.SmsCoupon;
import com.example.mybatis.repository.SmsCouponDAO;
import com.example.mybatis.service.SmsCouponService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author shanks on 2019-11-09
 */
@Service
public class SmsCouponServiceImpl implements SmsCouponService {

    @Autowired
    private SmsCouponDAO smsCouponDAO;

    @Override
    public SmsCoupon querySmsCouponDetail(int id) {
        SmsCoupon smsCoupon = smsCouponDAO.queryById(id);
        return smsCoupon;
    }
}
