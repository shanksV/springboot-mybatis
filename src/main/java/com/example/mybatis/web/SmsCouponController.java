package com.example.mybatis.web;

import com.example.mybatis.domain.SmsCoupon;
import com.example.mybatis.service.SmsCouponService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author shanks on 2019-11-09
 */
@RestController
@RequestMapping("/voucher/web/")
public class SmsCouponController {

    @Autowired
    SmsCouponService smsCouponService;

    @RequestMapping("/querySmsCouponDetail")
    public SmsCoupon querySmsCouponDetail(@RequestParam("id") int id){
        SmsCoupon smsCoupon = smsCouponService.querySmsCouponDetail(id);
        return smsCoupon;
    }
}
