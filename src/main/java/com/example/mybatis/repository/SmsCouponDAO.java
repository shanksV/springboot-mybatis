package com.example.mybatis.repository;

import com.example.mybatis.domain.SmsCoupon;
import org.apache.ibatis.annotations.Param;

/**
 * @author shanks on 2019-11-09
 */

public interface SmsCouponDAO {


    SmsCoupon queryById(@Param("id") Integer id);
}
