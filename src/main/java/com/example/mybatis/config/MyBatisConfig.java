package com.example.mybatis.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author shanks on 2019-11-09
 */
@Configuration
@MapperScan("com.example.mybatis.repository")
public class MyBatisConfig {
}
